//import Colyseus from "../../types/colyseus";

import Player from "./Player";
import Inputs from "./inputs";
import { moveSpeed, PlayerInput, Utils } from "./Utils";

const {ccclass, property} = cc._decorator;

@ccclass
export default class Game extends cc.Component {

  static instance: Game = null;
    @property
    private serverURL : string = "localhost";
    @property
    private port : string = "2567";

    @property(cc.Prefab)
    playerPrefab: cc.Prefab = null;

    @property(cc.Node) 
    root: cc.Node = null;

    private client : Colyseus.Client | null = null;
    private room : Colyseus.Room | null = null;
    
    private players: {[key: string]: Player} = {}
    private me: Player = null;
    
    actions: PlayerInput[] = [];

    onLoad() {
      Game.instance = this;
      
      let endpoint : string = `ws://${this.serverURL}:${this.port}`;
      this.client = new Colyseus.Client(endpoint);
    }

    start() {
      this.connect();
    }

    async connect () {
        
        console.log("@ Joining game...");
        this.room = await this.client!.joinOrCreate("my_room");

        this.room.state.players.onAdd = (att, id) => {
          
          console.log("player [", id, "] joined.... " );

          let isLocal = id === this.room.sessionId;
          // create player
          let player = cc.instantiate(this.playerPrefab).getComponent(Player);
          player.init(att, isLocal);

          att.onChange = () => {
            this.playerOnChange(att, id);
          }

          this.players[id] = player;
          this.root.addChild(player.node);

          // create local player
          if (isLocal) {
            this.me = cc.instantiate(this.playerPrefab).getComponent(Player);
            this.me.init(att, false);
            
            this.root.addChild(this.me.node);
          }
        }

        this.room.state.players.onRemove = (player, sessionId) => {
          this.players[sessionId].onDeath();
          delete this.players[sessionId];
          
          if (sessionId === this.room.sessionId && this.me) {
            this.me.onDeath();
            this.me = null;
          }
        }
    }


    playerOnChange(att: any, id: any) {
      let isLocal = id === this.room.sessionId;
      if (isLocal && this.me) {

        let ghost = this.players[id];
        if (att.ack !== this.me.ack) {
          this.me.ack = att.ack;

          ghost.x = att.x;
          ghost.y = att.y;
          ghost.toX = att.x;
          ghost.toY = att.y;

          let index = this.actions.findIndex((act) => act.ts === att.ack)
          this.actions = this.actions.slice(index + 1);
          this.actions.forEach((act) => {
            let pos = this.simulateMove(ghost.x, ghost.y, act.value, moveSpeed);
            ghost.x = pos.x;
            ghost.y = pos.y;
            ghost.toX = pos.x;
            ghost.toY = pos.y
          })

          let dist = Utils.getDistance(this.me.x, this.me.y, ghost.x, ghost.y);
          if (dist > 0) {
            this.me.x = ghost.x;
            this.me.y = ghost.y;
          }
        }
      }
      else {
        let player = this.players[id];
        player.x = player.toX;
        player.y = player.toY;
        player.toX = att.x;
        player.toY = att.y;
      }
    }

    update() {
      // process input
      this.processInput();

      //update position of all player
      for(let id in this.players) {
        this.players[id].updatePosition();
      }

      //TODO other update?
    }

    simulateMove(x: number, y: number, dir: cc.Vec2, speed: number): {x: number, y: number} {
      let mag = Math.sqrt(dir.x * dir.x + dir.y * dir.y);
      let dx = Math.round(dir.x * speed / mag);
      let dy = Math.round(dir.y * speed / mag);
      x += dx;
      y += dy;
      return {x, y};
    }
    
    processInput() {

      if (Inputs.instance.isKeyPressed) {
        let value = Inputs.instance.moveDir;

        let message = {
          ts: Date.now(),
          value
        }

        this.room!.send("move", message);
        
        this.actions.push(message);

        this.me.move(value.x, value.y, moveSpeed);
      }
    }
}
