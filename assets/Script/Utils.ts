export class Utils {
    public static lerp(a: number, b: number, n: number): number {
        return (1 - n) * a + n * b;
    }

    public static getDistance(x1: number, y1: number, x2: number, y2: number): number {
        return Math.hypot(x2 - x1, y2 - y1);
    }
}

export const frameTime = 1000 / 60; // millisecond
export const avgLagTime = 50;       // millisecond
export const moveSpeed = 3;

export interface PlayerInput {
    value?: any;
    ts: number;
  }