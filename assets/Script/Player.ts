// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import { avgLagTime, frameTime, Utils } from "./Utils";

const {ccclass, property} = cc._decorator;

@ccclass
export default class Player extends cc.Component {

    @property(cc.Sprite)
    display: cc.Sprite = null;

    private colors: cc.Color[] = [cc.Color.RED, cc.Color.GREEN, cc.Color.YELLOW, cc.Color.BLUE, cc.Color.CYAN, cc.Color.MAGENTA];
    
    private _ghost: boolean = false;
    set isGhost(isGhost: boolean) {
        this._ghost = isGhost;
        if (isGhost) this.display.enabled = false; 
    }
    get isGhost() {
        return this._ghost;
    }

    //interpolate param
    toX: number;
    toY: number;
    ack: number;
    set x(x: number) { this.node.x = x; }
    get x() { return this.node.x; }
    set y(y: number) { this.node.y = y; }
    get y() { return this.node.y; }

    
    init(att: any, ghost: boolean) {
        this.x = att.x;
        this.y = att.y;
        this.toX = att.x;
        this.toY = att.y;
        this.node.color = this.colors[att.color];
        this.isGhost = ghost;
    }

    onDeath() {
        this.node.removeFromParent();
    }

    move(x: number, y: number, speed: number) {
        let mag = Math.sqrt(x * x + y * y);
        let dx = Math.round(x * speed / mag);
        let dy = Math.round(y * speed / mag);

        this.x += dx;
        this.y += dy;
    }
    
    updatePosition() {
        let dist = Utils.getDistance(this.x, this.y, this.toX, this.toY);
        if (dist > 0.01) {
            this.x = Utils.lerp(this.x, this.toX, frameTime / avgLagTime);
            this.y = Utils.lerp(this.y, this.toY, frameTime / avgLagTime);
        }
    }
}
