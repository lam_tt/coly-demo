// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class Inputs extends cc.Component {

    static instance: Inputs = null;

    moveDir: cc.Vec2 = cc.v2(0, 0);

    onLoad() {
        Inputs.instance = this;
    }

    start() {
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
    }

    onKeyDown(event: cc.Event.EventKeyboard) {
        
        if (event.keyCode === cc.macro.KEY.w) this.moveDir.y = 1;
        if (event.keyCode === cc.macro.KEY.s) this.moveDir.y = -1;

        if (event.keyCode === cc.macro.KEY.a) this.moveDir.x = -1;
        if (event.keyCode === cc.macro.KEY.d) this.moveDir.x = 1;
    }

    onKeyUp(event: cc.Event.EventKeyboard) {
    
        if (event.keyCode === cc.macro.KEY.w) this.moveDir.y = 0;
        if (event.keyCode === cc.macro.KEY.s) this.moveDir.y = 0;

        if (event.keyCode === cc.macro.KEY.a) this.moveDir.x = 0;
        if (event.keyCode === cc.macro.KEY.d) this.moveDir.x = 0;
    }

    get isKeyPressed() {
        return !(this.moveDir.y === 0 && this.moveDir.x === 0);
    }
}


