"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MyRoom = exports.State = exports.Player = void 0;
const colyseus_1 = require("colyseus");
const schema_1 = require("@colyseus/schema");
class Player extends schema_1.Schema {
    constructor() {
        super(...arguments);
        this.x = 100 + Math.floor(Math.random() * 441);
        this.y = 50;
        this.speed = 100;
        this.playerInput = [];
    }
    processInput() {
        while (this.playerInput.length > 0) {
            let data = this.playerInput.shift();
            if (data.x) {
                this.x += this.speed * data.x * data.dt;
            }
            else if (data.y) {
                this.y += this.speed * data.y * data.dt;
            }
        }
    }
}
__decorate([
    schema_1.type("number")
], Player.prototype, "x", void 0);
__decorate([
    schema_1.type("number")
], Player.prototype, "y", void 0);
exports.Player = Player;
class State extends schema_1.Schema {
    constructor() {
        super(...arguments);
        this.players = new schema_1.MapSchema();
    }
    createPlayer(sessionId) {
        this.players.set(sessionId, new Player());
    }
    removePlayer(sessionId) {
        this.players.delete(sessionId);
    }
    movePlayer(sessionId, data) {
        this.players.get(sessionId).playerInput.push(data);
        console.log(sessionId, " -> playerInput.length", this.players.get(sessionId).playerInput.length);
    }
    processInput() {
        this.players.forEach((player, id) => {
            player.processInput();
        });
    }
}
__decorate([
    schema_1.type({ map: Player })
], State.prototype, "players", void 0);
exports.State = State;
class MyRoom extends colyseus_1.Room {
    onCreate(options) {
        this.setState(new State());
        this.onMessage("move", (client, message) => {
            this.state.movePlayer(client.sessionId, message);
        });
    }
    onJoin(client, options) {
        console.log(client.sessionId, "joined!");
        this.state.createPlayer(client.sessionId);
        if (this.state.players.size === 2) {
            this.lock();
            this.startGame();
        }
    }
    onLeave(client, consented) {
        console.log(client.sessionId, "left!");
        this.state.removePlayer(client.sessionId);
    }
    onDispose() {
        console.log("room", this.roomId, "disposing...");
    }
    startGame() {
        let t = Date.now();
        this.clock.setInterval(() => {
            let dt = (Date.now() - t) / 1000;
            t = Date.now();
            this.update(dt);
        }, 1000 / 60);
    }
    update(dt) {
        this.state.processInput();
    }
}
exports.MyRoom = MyRoom;
