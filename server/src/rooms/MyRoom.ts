import { Room, Client } from "colyseus";
import { type, Schema, MapSchema } from "@colyseus/schema";

export interface PlayerInput {
  id?: string;
  value?: any;
  ts: number;
}

export class Player extends Schema {
  @type("number") x: number = 100 + Math.floor(Math.random() * 441);
  @type("number") y: number = 50;
  @type("number") ack: number;
  @type("number") color: number;

  speed: number = 3;

  constructor() {
    super();
    this.color = Math.floor(Math.random() * 6);
  }

  move(x: number, y: number) {
    let mag = Math.sqrt(x * x + y * y);
    let dx = Math.round(x * this.speed / mag);
    let dy = Math.round(y * this.speed / mag);

    this.x += dx;
    this.y += dy;
  }
}

export class State extends Schema {
  @type({map: Player}) players = new MapSchema<Player>();

  actions: PlayerInput[] = []

  createPlayer(sessionId: string) {
    this.players.set(sessionId, new Player());
  } 

  removePlayer(sessionId: string) {
    this.players.delete(sessionId);
  }

  onActionMove(data: PlayerInput) {
    this.actions.push(data);
  }

  processInput() {
    while(this.actions.length > 0) {
      let action = this.actions.shift();
      this.movePlayer(action.id, action.ts, action.value);
    }
  }

  movePlayer(id: string, ts: number, dir: any) {
    let player = this.players.get(id);
    player.move(dir.x, dir.y);
    player.ack = ts;
  }
}



export class MyRoom extends Room<State> {

  onCreate (options: any) {
    this.setState(new State());
    this.setSimulationInterval(() => this.updateGame())
    this.onMessage("move", (client, message) => {
      this.state.onActionMove({id: client.sessionId, ...message});
    });
	
  }

  onJoin (client: Client, options: any) {
    console.log(client.sessionId, "joined!");
    this.state.createPlayer(client.sessionId);

    if( this.state.players.size === 2) {
      this.lock();
    }
  }

  onLeave (client: Client, consented: boolean) {
    console.log(client.sessionId, "left!");
    this.state.removePlayer(client.sessionId);
  }

  onDispose() {
    console.log("room", this.roomId, "disposing...");
  }

  updateGame() {
    this.state.processInput();
  }
}
